package decorator.textreader;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Instance that defines, what methods are used.
 * <br>
 * In the Decorator-Pattern, this represents the Component,
 * a interface/abstract class describing what methods are used.
 * <br>
 * @author Kacper Urbaniec
 * @version 18.03.2019
 */
public interface TextReader {
  BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
  void write(String[] s);
  void read(String[] s);
}
